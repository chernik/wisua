import {One, Och, Ostl} from '../utils.js';
import {Component as ComponentModel, Instance} from '../models.js';
import {Okeys, Or} from '../objs.js';
import {TextMenu} from './Menu.js';

function getImgUrl(state, imgName){
    if(!imgName){
        return null;
    }
    const imgs = state.files.filter(
        img => img.name === imgName
    );
    if(imgs.length === 0){
        alert(`Not found file [${imgName}]`);
        return null;
    }
    return imgs[0].url;
}

function findComponents(state, name){
    return state.components.filter(
        comp => comp.name === name
    );
}

function addStyle(state, div, styles, {component, render}){
    if(styles.img){
        const url = getImgUrl(state, styles.img.name);
        if(url){
            Ostl(div, {
                backgroundImage: `url(${url})`,
                backgroundSize: '100% 100%',
            })
        }
    }
    if(styles.text){
        div.innerText = styles.text.text;
        // div.contenteditable = styles.text.editable;
        Ostl(div, {
            fontSize: '18px',
            color: '#151633',
            fontFamily: 'cursive',
            paddingLeft: '7px',
            paddingTop: '7px',
            boxSizing: 'border-box',
        });
    }
    if(styles.list){
        if(component.children.length === 0){
            div.innerText = 'Add a child for magic';
            return;
        }
        if(component.children.length > 1){
            div.innerText = 'Expected exact one child';
        }
        const data = component.children[0];
        data.x = data.y = 0;
        [data, ...Or(4).map(i => ({
            ...data,
            x:  styles.list.isVertical ? 0 : (i+1) * styles.list.step,
            y: !styles.list.isVertical ? 0 : (i+1) * styles.list.step,
        }))].map(render).forEach((el, i) => {
            Ostl(el, {
                opacity: `${100 - i * 85 / 5}%`,
            });
            Och(el, div);
        });
        return false;
    }
    return true;
}

function Component(gs, {data, component}, render){
    console.log('Component', gs, data, component, render);
    /*
    component: {
        width: number;
        height: number;
        children: [...];
    }
    data: {
        x: number;
        y: number;
        upd: never;
        styles: {...};
    }
    */
    const UI = {};
    UI.div = One('div');
    Ostl(UI.div, {
        width: `${component.width}px`,
        height: `${component.height}px`,
        position: 'absolute',
    });
    function upd(){
        Ostl(UI.div, {
            left: `${data.x}px`,
            top: `${data.y}px`,
        });
    }
    upd();
    data.upd = data.upd || [];
    data.upd.push(upd);
    if(addStyle(gs, UI.div, data.styles, {component, render})){
        console.log(component.children);
        component.children.map(render).forEach(el => {
            Och(el, UI.div);
        });
    }
    return {r: () => UI.div};
}

function DragComponent(gs, state, render, {onMove, onProps, onRemove, onStyles}){
    // console.log('DragComponent', gs, state, render);
    const component = Component(gs, state, render);
    const div = component.r();
    Ostl(div, {
        border: '1px solid blue',
    });
    div.oncontextmenu = e => {
        if(e.ctrlKey){
            onRemove();
        } else if(e.shiftKey){
            onStyles();
        } else {
            onProps();
        }
        e.stopPropagation();
        e.preventDefault();
    }
    function upd(){
        Ostl(div, {
            left: `${state.data.x}px`,
            top: `${state.data.y}px`,
        });
        state.data.upd?.forEach(cb => cb());
    }
    const lastCoor = [null];
    div.onmousedown = e => {
        lastCoor[0] = [e.clientX, e.clientY];
        window.addEventListener('mousemove', mouseMove);
        window.addEventListener('mouseup', mouseUp);
    }
    function mouseUp(){
        lastCoor[0] = null;
        window.removeEventListener('mousemove', mouseMove);
        window.removeEventListener('mouseup', mouseUp);
    }
    function mouseMove(e){
        const newCoor = [e.clientX, e.clientY];
        state.data.x += newCoor[0] - lastCoor[0][0];
        state.data.y += newCoor[1] - lastCoor[0][1];
        lastCoor[0] = [e.clientX, e.clientY];
        onMove();
        upd();
    }
    div.onclick = e => e.stopPropagation();
    div.onselectstart = () => false;
    return component;
}

function RootComponent(gs, state, render){
    const component = Component(gs, state, render);
    Ostl(component.r(), {
        margin: '20px',
        border: '1px solid black',
        left: 'auto',
        top: 'auto',
    });
    return component;
}

function errorComponent({data}, render, msg){
    // console.log('errorComponent', data, render, msg);
    const state = {
        data,
        component: ComponentModel({
            width: 100,
            height: 100,
        }),
    };
    const div = render(state);
    Ostl(div, {
        backgroundColor: 'red',
    });
    div.innerText = msg;
    return div;
}

function recurseComponent({data, component: baseComponent}, render){
    // console.log('recurseComponent', data, baseComponent, render);
    const state = {
        data,
        component: ComponentModel({
            width: baseComponent.width,
            height: baseComponent.height,
        }),
    };
    const div = render(state);
    Ostl(div, {
        background: 'radial-gradient(ellipse, #fff 0%, #000 30%, #fff0 60%)',
    });
    div.title = 'Recursion detected';
    return div;
}

export default (state, onUpdate) => {
    const UI = {};
    UI.div = One('div');
    UI.render = (data, render) => {
        console.log('render', data, render);
        render = render || (subState => Component(
            state, subState,
            data => UI.render(data)
        ).r());
        const {name} = data;
        const components = findComponents(state, name);
        if(components.length === 0){
            return errorComponent({data}, render, `Not found [${name}]`);
        }
        if(components.length === 2){
            return errorComponent({data}, render, `Duplicate [${name}]`);
        }
        const subState = {data, component: components[0]};
        if(state.graph.has(name)){
            if(state.loopCount > 7){
                return recurseComponent(subState, render);
            }
            state.loopCount++;
        }
        state.graph.add(name);
        const bufRec = {
            graph: Array.from(state.graph),
            loopCount: state.loopCount,
        };
        const div = render(subState);
        state.graph = new Set(bufRec.graph);
        state.loopCount = bufRec.loopCount;
        return div;
    };
    UI.upd = () => {
        if(state.choose === undefined){
            UI.div.innerText = '<< Select component';
            return;
        }
        state.graph = new Set();
        state.loopCount = 0;
        const component = state.components[state.choose];
        const rootData = Instance(
            component,
            0, 0
        );
        UI.div.innerHTML = '';
        const mainComp = UI.render(rootData, rootState =>
            RootComponent(state, rootState, dragData =>
                UI.render(dragData, dragState =>
                    DragComponent(
                        state, dragState,
                        data => UI.render(data),
                        {
                            onProps(){
                                state.forEditor = {obj: dragData, type: 'Instance'};
                                onUpdate();
                            },
                            onRemove(){
                                const idx = component.children.findIndex(
                                    adata => adata === dragData
                                );
                                if(idx < 0){
                                    return;
                                }
                                component.children.splice(idx, 1);
                                UI.upd();
                                onUpdate();
                            },
                            async onStyles(){
                                const styles = Okeys(dragData.styles);
                                if(styles.length === 0){
                                    alert('There is no styles');
                                    return;
                                }
                                const idx = await TextMenu(styles)();
                                if(idx === null){
                                    return;
                                }
                                const style = dragData.styles[styles[idx]];
                                state.forEditor = {obj: style, type: 'Style'};
                                onUpdate();
                            },
                            onMove: onUpdate,
                        }
                    ).r()
                )
            ).r()
        );
        delete state.graph;
        delete state.loopCount;
        mainComp.onclick = () => {
            state.forEditor = {obj: component, type: 'Component'};
            onUpdate();
        }
        mainComp.oncontextmenu = e => {
            const name = prompt('>');
            if(name){
                const components = findComponents(state, name);
                component.children.push(Instance(
                    components.length === 1
                        ? components[0]
                        : ComponentModel({name}),
                    e.offsetX,
                    e.offsetY
                ));
                UI.upd();
                onUpdate();
            }
            e.preventDefault();
        }
        Och(mainComp, UI.div);
    };
    UI.upd();
    return {r: () => UI.div, upd: UI.upd};
}
