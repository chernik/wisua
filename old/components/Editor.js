import {One, Ostl} from '../utils.js';
import {connectLens, jsonLens, lazyLens, triggerFieldLens} from '../lens.js';
import {simplifyObj} from '../objs.js';

function Editor(onUpdate){
    const div = One('div');
    const divEdit = One('div', div);
    Ostl(divEdit, {
        width: '100%',
        height: '100%',
    });
    divEdit.addEventListener('keyup', () => {
        onUpdate();
    });
    const editor = ace.edit(divEdit);
    editor.getSession().setMode('ace/mode/json');
    const lens = {
        s: code => editor.setValue(code),
        g: () => editor.getValue(),
    };
    return {r: () => div, lens};
}

export default (state, onUpdate) => {
    const editor = Editor(() => handleUpdate());
    const loc = {
        lens: null,
        prevType: '',
        isModelUpdates: false,
    };
    function handleUpdate(){
        if(!loc.lens || !loc.lens.g()){
            return;
        }
        if(loc.isModelUpdates){
            state.forEditor.obj.modelUpdates.upd();
            loc.isModelUpdates = false;
        }
        onUpdate();
    }
    function updateLens(){
        if(loc.lens && loc.prevType === state.forEditor.type){
            return;
        }
        loc.lens = connectLens([
            lazyLens(simplifyObj),
            ...(
                state.forEditor.type === 'Component'
                    ? [triggerFieldLens('styles', () => {
                        loc.isModelUpdates = true;
                    })]
                    : []
            ), jsonLens, editor.lens
        ]);
        loc.prevType = state.forEditor.type;
    }
    function upd(){
        if(state.forEditor === undefined){
            editor.lens.s('["Enter someth"]');
            return;
        }
        updateLens();
        loc.lens.s(state.forEditor.obj);
    }
    return {
        r: editor.r,
        upd,
    };
}
