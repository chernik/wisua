import {One, Och, Ostl, openFile, getImgData} from '../utils.js';
import {Component} from '../models.js';

function simpleBut(el, t, cb){
    const but = One('button', el);
    but.innerText = t;
    Ostl(but, {
        width: '20px',
        height: '20px',
        marginLeft: '3px',
    });
    but.onclick = cb;
    return but;
}

function LeftPanelItem(state, {onChoose, onRemove, onUpdate, isChoose}){
    // {name: string}
    const UI = {};
    UI.div = One('div');
    Ostl(UI.div, {
        display: 'flex',
        marginTop: '2px',
    });
    UI.name = One('span', UI.div);
    Ostl(UI.name, {
        width: '100%',
        borderBottom: '1px solid black',
    });
    if(isChoose){
        Ostl(UI.name, {
            backgroundColor: '#dddddd',
        });
    }
    UI.name.onclick = () => {
        onChoose();
    };
    UI.name.oncontextmenu = e => {
        const newName = prompt('>', state.name);
        if(newName){
            state.name = newName;
            UI.upd();
            onUpdate();
        }
        e.preventDefault();
    };
    simpleBut(UI.div, 'X', () => {
        onRemove();
    });
    UI.upd = () => {
        UI.name.innerText = state.name;
    };
    UI.upd();
    return {r: () => UI.div};
}

function NewItem(state, onNew){
    // {components: []}
    const UI = {};
    UI.div = One('div');
    simpleBut(UI.div, '+', () => {
        state.components.push(Component());
        onNew();
    });
    simpleBut(UI.div, 'P', async () => {
        const files = await openFile({accept: ['png']});
        const img = await getImgData(files[0]);
        if(img === null){
            return;
        }
        state.files.push(img);
        state.components.push(Component({
            styles: 'img',
            staticStyles: {
                img: {name: img.name},
            },
            width: img.width,
            height: img.height,
        }));
        onNew();
    });
    return {r: () => UI.div};
}

export default (state, onUpdate) => {
    // {components: [], choose: number}
    const UI = {};
    UI.div = One('div');
    UI.newItem = NewItem(state, () => {
        UI.upd();
        onUpdate();
    }).r();
    Och(UI.newItem, UI.div);
    UI.itemsCont = One('div', UI.div);
    UI.upd = () => {
        UI.itemsCont.innerHTML = '';
        state.components.forEach((component, i) => {
            const item = LeftPanelItem(component, {
                onChoose(){
                    state.choose = i;
                    UI.upd();
                    onUpdate();
                },
                onRemove(){
                    state.components.splice(i, 1);
                    if(i === state.choose){
                        delete state.choose;
                    }
                    if(state.choose && i < state.choose){
                        state.choose--;
                    }
                    UI.upd();
                    onUpdate();
                },
                onUpdate,
                isChoose: i === state.choose,
            }).r();
            Och(item, UI.itemsCont);
        });
    }
    return {r: () => UI.div, upd: UI.upd};
}
