import {One, Och, Ostl} from '../utils.js';
import LeftPanel  from './LeftPanel.js';
import Canva from './Canva.js';
import Editor from './Editor.js';

export default state => {
    const UI = {};
    UI.parent = One('div');
    Ostl(UI.parent, {
        display: 'flex',
        height: '100%',
    });
    UI.childs = [];
    function addChild(Child, width){
        const child = Child(state, () =>
            UI.childs.forEach(ch => ch !== child && ch.upd())
        );
        UI.childs.push(child);
        const div = child.r();
        Och(div, UI.parent);
        Ostl(div, {width});
    }
    addChild(LeftPanel, '170px');
    addChild(Canva, 'calc(100% - 470px)');
    addChild(Editor, '300px');
    return {r: () => UI.parent};
}
