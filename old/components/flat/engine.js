function e2c(e){
    return {
        x: e.offsetX,
        y: e.offsetY,
    };
}

export function createGame(ps){
    /*
    ps: {
        size: {w, h},
        events: {
            key(isDown, key, obj),
        },
    }
    */

    /*
    returns {
        div,
        offerObj(obj),
    }
    */

    // UI
    const ctx = document.createElement('canvas').getContext('2d');
    ctx.canvas.style.marginTop = ctx.canvas.style.marginLeft = '50px';
    ctx.canvas.width = ps.size.w;
    ctx.canvas.height = ps.size.h;
    ctx.canvas.tabIndex = -1;

    // Center point
    const offset = {x: 0, y: 0};

    // Moving delta
    const delta = {x: 0, y: 0};

    // Gird size
    let SIZE = 5;

    setInterval(() => {
        offset.y -= delta.y * 20;
        offset.x -= delta.x * 20;
    }, 50);

    // New obj
    const currentObj = [null];
    // Mouse position on canvas
    const currentMouse = [null];

    // Initial objs
    const objs = [];

    function getLoc(){
        return {
            x: Math.floor((currentMouse[0].x - offset.x) / SIZE / 10),
            y: Math.floor((currentMouse[0].y - offset.y) / SIZE / 10),
        };
    }

    // Render
    function drawObjs(){
        // Clear
        ctx.canvas.width += 0;
        // Draw all objs
        objs.forEach(o => {
            o.draw(ctx, SIZE, offset);
        });
        // New object placed
        if(currentMouse[0] && currentObj[0]){
            const loc = getLoc();
            currentObj[0].l.x = loc.x;
            currentObj[0].l.y = loc.y;
            currentObj[0].draw(ctx, SIZE, offset);
        }
        // Border
        ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
        // Draw
        ctx.stroke();
    }

    function getObj(loc){
        return objs.filter(o =>
            o.l.x <= loc.x &&
            o.l.y <= loc.y &&
            (o.l.x + o.l.w) > loc.x &&
            (o.l.y + o.l.h) > loc.y
        );
    }

    function getMouseObj(){
        if(!currentMouse[0]){
            return [];
        }

        return getObj(getLoc());
    }

    function addObj(obj){
        objs.push(obj);
    }

    function removeObj(obj){
        return objs.splice(objs.findIndex(o => o === obj), 1)[0]
    }

    function handle(key, f){
        function boch(k, v){
            if(f){
                delta[k] = v;
                return;
            }
            if(delta[k] !== v){
                return;
            }
            delta[k] = 0;
        }
        switch(key){
            case 'w': {
                boch('y', -1);
                break;
            } case 's': {
                boch('y', 1);
                break;
            } case 'a': {
                boch('x', -1);
                break;
            } case 'd': {
                boch('x', 1);
                break;
            } case 'g': {
                if(!f || currentObj[0]){
                    break;
                }
                const mouseObjs = getMouseObj();
                if(mouseObjs[0]){
                    currentObj[0] = removeObj(mouseObjs[0]);
                }
                break;
            } default: {
                ps.events.key?.(f, key, getMouseObj());
            }
        }
    }

    ctx.canvas.onkeydown = e => handle(e.key, true);
    ctx.canvas.onkeyup = e => handle(e.key, false);

    ctx.canvas.onmouseover = e => {
        currentMouse[0] = e2c(e);
    }

    ctx.canvas.onmouseout = () => {
        currentMouse[0] = null;
    }

    ctx.canvas.onmousemove = e => {
        const c = e2c(e);
        currentMouse[0].x = c.x;
        currentMouse[0].y = c.y;
    }

    ctx.canvas.onclick = () => {
        if(!currentObj[0]) return;
        if(getObj(currentObj[0].l).length > 0) return;
        addObj(currentObj[0]);
        currentObj[0] = null;
    }

    ctx.canvas.onwheel = e => {
        const d = Math.pow(2, e.wheelDelta / 300);
        SIZE *= d;
        offset.x = (offset.x - currentMouse[0].x) * d + currentMouse[0].x;
        offset.y = (offset.y - currentMouse[0].y) * d + currentMouse[0].y;
    }

    setInterval(drawObjs, 10);

    return {
        div: ctx.canvas,
        offerObj(o){
            currentObj[0] = o;
        },
        getObjs(){
            return objs;
        },
        setObjs(newObjs){
            objs.splice(0, objs.length);
            objs.push(...newObjs);
        },
        getObj,
        addObj,
        removeObj,
    };
}
