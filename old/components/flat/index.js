import { createGame } from './engine.js';
import {lifePs} from './life.js';
import { TextObj } from './objs.js';

function keysPs(game, ps){
    function createObj(key){
        const o = TextObj({
            x: 0, y: 0, w: 1, h: 1,
        }, key);
        game.offerObj(o);
    }

    ps.events = {
        ...(ps.events ?? {}),
        key(f, key){
            f && createObj(key);
        },
    };
}

function onKeyPs(_game, ps, needKey, cb){
    const prev = ps.events?.key;
    ps.events ??= {};
    ps.events.key = (f, key, o) => {
        if(key === needKey && cb(f, o)){
            return;
        }
        prev?.(f, key, o);
    };
}

const patternObjLens = {
    p2o(p, t){
        return [
            TextObj({x: 0, y: 0, w: 1, h: 1}, t),
            ...p.map(po => TextObj({x: po.x, y: po.y, w: 1, h: 1}, po.t)),
        ];
    },
    o2p(os){
        return os.slice(1).map(o => ({x: o.l.x - os[0].l.x, y: o.l.y - os[0].l.y, t: o.t}));
    },
};

window.onload = () => {
    const allPs = {
        main: {
            size: {w: 500, h: 700},
        },
        pre: {
            size: {w: 300, h: 300},
        },
        post: {
            size: {w: 300, h: 300},
        },
    };
    const allGames = {
        main: createGame(allPs.main),
        pre: createGame(allPs.pre),
        post: createGame(allPs.post),
    };
    const mainCol = document.createElement('div');
    mainCol.appendChild(allGames.main.div);
    const leftCol = document.createElement('div');
    leftCol.appendChild(allGames.pre.div);
    leftCol.appendChild(allGames.post.div);
    const but = document.createElement('button');
    but.innerText = 'Save';
    leftCol.appendChild(but);
    const div = document.createElement('div');
    div.classList.add('cols');
    div.appendChild(mainCol);
    div.appendChild(leftCol);
    for(let k in allPs){
        keysPs(allGames[k], allPs[k]);
    }
    lifePs(allGames.main, allPs.main);
    const patterns = {};
    const currentPattern = [null];
    onKeyPs(allGames.main, allPs.main, 'q', (isDown, mouseObjs) => {
        if(!isDown || !mouseObjs[0]) return;
        const o = mouseObjs[0];
        const pattern = patterns[o.t] ?? {t: o.t};
        const preObjs = patternObjLens.p2o(pattern.pre ?? [], pattern.t);
        const postObjs = patternObjLens.p2o(pattern.post ?? [], pattern.t);
        allGames.pre.setObjs(preObjs);
        allGames.post.setObjs(postObjs);
        currentPattern[0] = pattern;
        return true;
    });
    but.onclick = () => {
        if(!currentPattern[0]) return;
        const pattern = currentPattern[0];
        const preObjs = allGames.pre.getObjs();
        const postObjs = allGames.post.getObjs();
        pattern.pre = patternObjLens.o2p(preObjs);
        pattern.post = patternObjLens.o2p(postObjs);
        patterns[pattern.t] ??= pattern;
    }
    const range = document.createElement('input');
    range.type = 'range';
    leftCol.appendChild(document.createElement('br'));
    leftCol.appendChild(range);
    let timeout = 500;
    range.onchange = e => {
        timeout = +e.target.value * 10;
    };
    function restart(){
        allGames.main.findCross(patterns);
        setTimeout(restart, timeout);
    }
    restart();
    document.body.appendChild(div);
}
