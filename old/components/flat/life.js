import { TextObj } from "./objs.js";

export function lifePs(game, _ps){
    const objs = game.getObjs();
    function findCross(patterns){
        const preFinds = [];
        for(let o of objs){
            const pattern = patterns[o.t];
            if(!pattern) continue;
            const cands = [];
            for(let po of pattern.pre){
                const cand = objs.filter(o2 => {
                    if(o2.t !== po.t) return false;
                    if(o2.l.x - o.l.x !== po.x) return false;
                    if(o2.l.y - o.l.y !== po.y) return false;
                    return true;
                });
                if(cand.length === 0) break;
                console.assert(cand.length === 1);
                cands.push(cand[0]);
            }
            if(cands.length !== pattern.pre.length) continue;
            preFinds.push({
                center: o,
                pattern,
                objs: cands,
            });
        }
        for(let preFind of preFinds){
            let iCan = true;
            const newOps = [];
            for(let po of preFind.pattern.post){
                const loc = {
                    x: preFind.center.l.x + po.x,
                    y: preFind.center.l.y + po.y,
                };
                const locObjs = game.getObj(loc);
                if(locObjs.length === 0){
                    newOps.push(po);
                    continue;
                }
                const locObj = locObjs[0];
                const sameObjI = preFind.objs.findIndex(o => o === locObj);
                if(sameObjI < 0){
                    iCan = false;
                    break;
                }
                preFind.objs.splice(sameObjI, 1);
            }
            if(!iCan){
                preFind.moves = [];
                continue;
            }
            /* preFind: {
                center: центральный объект свертки,
                pattern: {
                    t,
                    pre: {
                        x, y, t,
                    }[],
                    post: {
                        x, y, t,
                    }[],
                },
                objs: объекты на удаление,
            } */
            const from = {};
            preFind.objs.forEach(o => {
                from[o.t] ??= [];
                from[o.t].push(o);
            });
            newOps.forEach(op => {
                if(!from[op.t]?.length){
                    game.addObj(TextObj({
                        x: preFind.center.l.x + op.x,
                        y: preFind.center.l.y + op.y,
                        w: 1,
                        h: 1,
                    }));
                    return;
                }
                const o = from[op.t].shift();
                o.l.x = preFind.center.l.x + op.x;
                o.l.y = preFind.center.l.y + op.y;
            });
            for(let t in from){
                from[t].forEach(game.removeObj);
            }
        }
    }
    game.findCross = findCross;
}
