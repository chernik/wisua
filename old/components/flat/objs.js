const imgs = {};

async function loadImage(id){
    if(imgs[id]){
        return imgs[id];
    }
    const im = new Image();
    im.src = `img/${id}`;
    im.style.display = 'none';
    document.body.appendChild(im);
    await new Promise(cb => im.onload = im.onerror = cb);
    document.body.removeChild(im);
    imgs[id] = im;
    return im;
}

export function Obj(l){
    return {
        l,
        async draw(ctx, SIZE, offset){
            const im = await loadImage('block.jpeg');
            ctx.drawImage(
                im,
                offset.x + l.x * SIZE * 10 + SIZE / 2,
                offset.y + l.y * SIZE * 10 + SIZE / 2,
                l.w * SIZE * 10 - SIZE,
                l.h * SIZE * 10 - SIZE
            );
            ctx.rect(
                offset.x + l.x * SIZE * 10 + SIZE / 2,
                offset.y + l.y * SIZE * 10 + SIZE / 2,
                l.w * SIZE * 10 - SIZE,
                l.h * SIZE * 10 - SIZE
            );
            ctx.stroke();
        }
    };
}

export function TextObj(l, t){
    const o = Obj(l);
    return {
        ...o,
        t,
        async draw(ctx, SIZE, offset){
            await o.draw(ctx, SIZE, offset);
            ctx.fillText(
                t,
                offset.x + l.x * SIZE * 10 + SIZE * 2,
                offset.y + l.y * SIZE * 10 + SIZE * 3
            );
        }
    }
}
