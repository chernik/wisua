import {flatCopy} from './objs.js';

function connectTwoLens(l1, l2){
    return {
        s(o){
            const r = l1.s(o);
            return r === null ? null : l2.s(r);
        },
        g(o){
            const r = l2.g(o);
            return r === null ? null : l1.g(r);
        },
    };
}

export function connectLens(lenses){
    if(lenses.length === 1){
        return lenses[0];
    }
    let lens = null;
    for(let i = lenses.length - 1; i > 0; i--){
        lens = lens === null
            ? connectTwoLens(lenses[i-1], lenses[i])
            : connectTwoLens(lenses[i-1], lens);
    }
    return lens;
}

export const jsonLens = {
    s: obj => JSON.stringify(obj, null, 4),
    g(s){
        try {
            return JSON.parse(s);
        } catch(e) {
            return null;
        }
    }
}

export function lazyLens(filter){
    const buf = [];
    return {
        s(obj){
            const copy = filter ? filter(obj) : obj;
            if(buf.length !== 0 && JSON.stringify(copy) === buf[1]){
                return null;
            }
            buf[0] = obj;
            buf[1] = JSON.stringify(copy);
            return copy;
        },
        g(obj){
            if(buf.length === 0){
                return null;
            }
            if(flatCopy(obj, buf[0]) === null){
                return null;
            }
            buf[1] = JSON.stringify(obj);
            return buf[0];
        },
    }
}

export function triggerFieldLens(field, trigger){
    const buf = [];
    return {
        s(obj){
            buf[0] = obj[field];
            return obj;
        },
        g(obj){
            if(buf.length > 0 && buf[0] !== obj[field]){
                trigger(buf[0], obj[field]);
                buf[0] = obj[field];
            }
            return obj;
        }
    }
}
