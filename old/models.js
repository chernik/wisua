import {flatCopy, updeps} from './objs.js';

function Style(style, props){
    if(style === 'img'){
        return {
            name: props?.name || 'not_found.png',
        };
    }
    if(style === 'text'){
        return {
            text: props?.text || 'Some text',
            editable: props?.editable || false,
        };
    }
    if(style === 'list'){
        return {
            step: 50,
            isVertical: true,
        };
    }
    return {wrongStyle: 'So wrong!'};
}

function copyStyles(s, d){
    return flatCopy(s, d, {
        canAddRemove: true,
        handleLevel: (s, d, k) => k === 'upd'
            ? false
            : flatCopy(s[k], d[k], {
                canAddRemove: true,
                canChange: false,
            }),
    });
}

export function Component(props){
    const component = {
        name: 'NewComponent',
        width: 300,
        height: 300,
        children: [],
        styles: '',
        staticStyles: {},
        ...(props || {}),
    };
    updeps(component, () => {
        const {staticStyles, styles} = component;
        const styleNames = styles
            .split(' ')
            .map(s => s.trim())
            .filter(s => s !== '');
        const realStyles = {};
        styleNames.forEach(s => {
            realStyles[s] = Style(s, staticStyles[s]);
        });
        copyStyles(realStyles, staticStyles);
    })();
    return component;
}

export function Instance(component, x, y){
    const data = {
        name: component.name,
        x, y,
        styles: {},
    };
    updeps(data, () => {
        const {staticStyles} = component;
        copyStyles(staticStyles, data.styles);
    })();
    component.modelUpdates.upds.push(data.modelUpdates.upd);
    return data;
}

export default {};
