export function OcpObj(o){
    return JSON.parse(JSON.stringify(o));
}

export function Okeys(o){
    const res = [];
    for(let k in o){
        res.push(k);
    }
    return res;
}

function OisT(obj, ts){
    return new Set(ts).has(typeof(obj));
}

const OisSimple = obj => OisT(obj, ['string', 'number', 'boolean']);

export function flatCopy(s, d, args){
    const {
        canAddRemove = false,
        handleLevel = () => false,
        canChange = true,
    } = args || {};
    const keys = Array.from(new Set([...Okeys(s), ...Okeys(d)]));
    return keys.filter(k => {
        if(s[k] === d[k]){
            return false;
        }
        if(s[k] === undefined){
            if(!canAddRemove){
                return false;
            }
            delete d[k];
            return true;
        }
        if(d[k] === undefined){
            if(!canAddRemove){
                return false;
            }
            if(OisSimple(s[k])){
                d[k] = s[k];
                return true;
            }
            d[k] = {};
            return handleLevel(s, d, k);
        }
        if(!canChange){
            return false;
        }
        if(typeof(s[k]) !== typeof(d[k])){
            return false;
        }
        if(!OisSimple(s[k])){
            return handleLevel(s, d, k);
        }
        d[k] = s[k];
        return true;
    }).length > 0 ? d : null;
}

export function simplifyObj(obj){
    const res = {};
    Okeys(obj).forEach(k => {
        if(!OisSimple(obj[k])){
            return;
        }
        res[k] = obj[k];
    });
    return res;
}

export function updeps(obj, onUpd){
    obj.modelUpdates = {
        upds: [],
    };
    obj.modelUpdates.upd = () => {
        onUpd();
        obj.modelUpdates.upds.forEach(upd => upd());
    }
    return onUpd;
}

export function Or(n){
    return [...Array(n).keys()];
}
