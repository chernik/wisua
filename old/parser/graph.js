import {strAction, LinkType, tabSpace, downloadContent, createBlobUrl, createBase64Url, getSourceMap} from './utils.js';
import MacrosFabric from './macroses/index.js';
import Gjs from './lang/js.js';
import {Or} from '../objs.js';

function ParseError(t, l, m){
    return {
        chernik: 'chernik',
        t, m, l,
    };
}

function shouldParseError(e){
    if(e.wisua !== 'wisua'){
        throw Error('fatal', `Raised non-error\n${e}`);
    }
}

function updateParseError(e, m, l = null){
    shouldParseError(e);
    e.l = l || e.l;
    e.m = `${e.m}\n${m}`;
    throw e;
}

function parseCode(code){
    return code
        // Get lines
        .split('\n')
        // Remove comments & strip & save line number
        .map((s, si) => [s.split('//')[0].trim(), si])
        // Remove empty lines
        .filter(line => !!line[0])
        // Get rule parts
        .map(s => [
            s[0]
                .split('=>')
                .map(s => s.trim()),
            s[1] // line number
        ]);
}

/**
 * 
 * @param {string} action Code of action
 * @returns Action
 */
function parseAction(action){
    if(action === ''){
        throw ParseError('syntax', null, 'Empty action');
    }
    const marki = action.split(':').map(s => s.trim());
    if(marki[0] === ''){
        throw ParseError('syntax', null, 'Empty mark of action. Try to use <nodes & labels> instead of :<nodes & labels>');
    }
    if(marki.length > 2){
        throw ParseError('syntax', null, 'Wrong <mark>:<notes & labels> format');
    }
    const [mark, snotesLabels] = marki.length === 1
        ? [null, marki[0]]
        : marki;
    const notesLabels = snotesLabels
        .split('.')
        .map(s => {
            const noteLabel = s.trim();
            if(noteLabel[0] !== '@'){
                return {note: noteLabel};
            }
            return {label: noteLabel
                .slice(1)
                .split('/')
                .map(s => s.trim())
                .filter(s => !!s)};
        });
    return {mark, notesLabels};
}

/**
 * 
 * @param {Action} a1 First action to compare
 * @param {Action} a2 Second action to compare
 * @returns boolean
 */
function isEqualActions(a1, a2){
    return strAction(a1) === strAction(a2);
}

const initAction = {
    mark: null,
    notesLabels: [{note: 'init'}],
};

/**
 * 
 * @param {string | null} func Code of function or null for => => identity
 * @returns Modifier
 */
function parseFunc(func){
    const sfunc = func || null;
    return {
        sfunc,
        apply: v => sfunc === null ? v : sfunc.replaceAll('$v', `(${v})`),
    };
}

/**
 * 
 * @param {ParsedRule[]} lines Parsed rules
 * @returns Rule[]
 */
function parseRules(lines){
    return lines.map(line => {
        if(line[0].length !== 3){
            if(line[0].length === 2){
                throw ParseError('syntax', line[1], 'Use <trigger> => => <consumer> for identity function', line[1]);
            }
            throw ParseError('syntax', line[1], 'Wrong <trigger> => <function> => <consumer> format');
        }
        const [[strigger, sfunc, sconsumer], lineNumber] = line;
        const rule = {lineNumber};
        try {
            rule.trigger = parseAction(strigger);
        } catch(e) {
            updateParseError(e, 'In trigger', lineNumber);
        }
        try {
            rule.consumer = parseAction(sconsumer);
        } catch(e) {
            updateParseError(e, 'In consumer', lineNumber);
        }
        rule.func = parseFunc(sfunc);
        return rule;
    });
}

/**
 * Returned rules which will call with given action
 * 
 * @param {Rule[]} rules Rules
 * @param {Action} action Action which triggered all graph
 * @returns Rule[]
 */
function getNext(rules, action, walked){
    const sa = strAction(action);
    if(walked[sa]){
        walked[sa]++;
        return null;
    }
    walked[sa] = 1;
    return rules.filter(({trigger}) => isEqualActions(action, trigger));
}

function getMacros(action, ctx){
    const macrosName = action.notesLabels[0]?.note;
    if(!macrosName){
        return null;
    }
    const Macros = MacrosFabric(macrosName);
    if(!Macros){
        return null;
    }
    return Macros(action, ctx);
}

/**
 * 
 * @param {Action} action Action to ejecting first label
 * @returns ActionLabel | null
 */
function getLabel(action){
    return action.notesLabels[0]?.label || null;
}

/**
 * Return lines to join
 * 
 * @param {Rule[]} rules Rules to walking
 * @param {Value} value Init value
 * @param {Action} action Action for first trigger
 * @param {Ctx} ctx magic
 * @returns string[]
 */
function strTraceRec(rules, value, action, ctx){
    const MAX_VALUE_LEN = 40;
    const resHeader = `${strAction(action)} <${value.slice(0, MAX_VALUE_LEN)}...>`;
    const triggeredRules = getNext(rules, action, ctx.walked);
    if(triggeredRules === null){
        return [ // Already triggered
            resHeader,
            tabSpace + '[circle]',
        ];
    }
    if(triggeredRules.length === 0){
        return [ // Noth triggered here
            resHeader,
            tabSpace + '[leaf]',
        ];
    }
    const label = getLabel(action);
    // Make next context with new label
    const newCtx = label ? {...ctx, label} : ctx;
    // Get lines to join
    const lines = triggeredRules.flatMap(rule => {
        const {func, consumer} = rule;
        // Can be macros
        const macros = getMacros(consumer, newCtx);
        // Propagate next value through Modifier
        const funcValue = func.apply(value);
        if(!macros){
            // If simple action
            return strTraceRec(
                rules, // Graph
                funcValue, // Next falue
                consumer, // Next trigger
                newCtx, // Next context
            );
        }
        const macrosValue = macros.apply(funcValue);
        const macrosHeader = [
            `>> ${strAction(consumer)} with <${funcValue.slice(0, MAX_VALUE_LEN)}...>`,
            `<< ${macrosValue.slice(0, MAX_VALUE_LEN)}... with type <${macros.type}>`,
        ];
        rule.macros = macros;
        if(!macros.action){
            // If macros wants to be leaf
            return [
                ...macrosHeader,
                tabSpace + '[macro leaf]'
            ];
        }
        // Propagate next value through macros
        return [
            ...macrosHeader,
            ...strTraceRec(
                rules, // Graph
                macrosValue, // Next value
                macros.action, // Action for macros enjoying
                newCtx, // Next ctx
            ),
        ];
    }).map(s => tabSpace + s);
    return [resHeader, ...lines];
}

function strWalkRec(rules, value, action, ctx){
    const {g} = ctx;
    const triggeredRules = getNext(rules, action, ctx.walked);
    if(triggeredRules === null){
        // Already walked
        const [valueId, labelId] = ctx.simpleActionJoin[strAction(action)];
        return [
            ...g.assign(valueId, value),
            ...g.comment(g.goto(labelId), `${strAction(action)} <${value}> [circle]`),
        ];
    }
    if(triggeredRules.length === 0){
        return [ // Noth triggered here
            ...g.comment(null, `${strAction(action)} <${value}> [leaf action]`),
        ];
    }
    let joinValueId = null;
    let joinHeader = [];
    if(ctx.alreadyWalked[strAction(action)] > 1){
        // Walked many times
        ctx.cnt.values++;
        ctx.cnt.labels++;
        // Join label
        const labelId = ctx.cnt.labels;
        // Join value
        const valueId = ctx.cnt.values;
        console.warn(strAction(action), ctx.simpleActionJoin[strAction(action)]);
        ctx.simpleActionJoin[strAction(action)] = ctx.simpleActionJoin[strAction(action)] ?? [valueId, labelId];
        joinHeader = [
            ...g.assign(valueId, value),
            ...g.goto(labelId),
            ...g.lbl(labelId),
        ];
        joinValueId = valueId;
    }
    function getJoinHeader(){
        if(joinHeader.length > 0){
            // Only one join definition
            const res = joinHeader;
            joinHeader = [];
            return res;
        }
        return joinHeader;
    }
    const label = getLabel(action);
    // Make next context with new label
    const newCtx = label ? {...ctx, label} : ctx;
    // Get lines to join
    const lines = triggeredRules.flatMap(rule => {
        const {func, consumer, lineNumber} = rule;
        // Get macros
        const macros = rule.macros;
        // Propagate next value through Modifier
        const funcValue = func.apply(
            joinValueId === null ? value : g.vari(joinValueId)
        );
        if(!macros){
            // If simple action
            if(joinValueId === null){
                // Skip graph lines
                return strWalkRec(
                    rules, // Graph
                    funcValue, // Next value
                    consumer, // Next trigger
                    newCtx, // Next context
                );
            }
            // If part of join
            return [
                ...getJoinHeader(),
                ...g.sourceMapMarker(lineNumber),
                ...strWalkRec(
                    rules,
                    funcValue,
                    consumer,
                    newCtx,
                ),
            ];
        }
        const macrosValue = macros.apply(funcValue);
        if(!macros.action){
            // If macros wants to be leaf
            return [
                ...getJoinHeader(),
                ...g.sourceMapMarker(lineNumber),
                ...g.comment(g.noret(macrosValue), '[leaf macros]'),
            ];
        }
        switch(macros.type){
            case LinkType.Multiple:
            case LinkType.Condition: {
                ctx.cnt.values++;
                ctx.cnt.labels += 2;
                const labelId = ctx.cnt.labels;
                const valueId = ctx.cnt.values;
                return [
                    ...getJoinHeader(),
                    ...g.sourceMapMarker(lineNumber),
                    ...g.assign(valueId, macrosValue),
                    ...g.takeEvery(valueId, labelId - 1),
                    ...g.goto(labelId),
                    ...g.lbl(labelId - 1),
                    ...strWalkRec(
                        rules,
                        g.varival(g.vari(valueId)),
                        macros.action,
                        newCtx,
                    ),
                    ...g.goto('L'),
                    ...g.lbl(labelId),
                ];
            } case LinkType.One: {
                ctx.cnt.values++;
                const valueId = ctx.cnt.values;
                return [
                    ...getJoinHeader(),
                    ...g.sourceMapMarker(lineNumber),
                    ...g.assign(valueId, macrosValue),
                    ...strWalkRec(
                        rules,
                        g.vari(valueId),
                        macros.action,
                        newCtx,
                    ),
                ];
            } default:
                break;
        }
        // Propagate next value through macros
        return [
            ...getJoinHeader(),
            ...g.sourceMapMarker(lineNumber),
            ...g.comment(null, `>> ${strAction(consumer)} with <${funcValue}>`),
            ...g.comment(null, `<< ${macrosValue} with type <${macros.type}>`),
            ...strWalkRec(
                rules, // Graph
                macrosValue, // Next value
                macros.action, // Action for macros enjoying
                newCtx, // Next ctx
            ),
        ];
    });
    return [
        ...g.comment(null, `${strAction(action)} <${value}>`),
        ...lines,
    ];
}

async function downloadBounder(n, g){
    const baseCode = await downloadContent(`parser/lang/${n}.bnd.js`);
    return args => {
        let newCode = baseCode;
        for(let karg in args){
            newCode = newCode
                .split(g.bndCmnt(karg))
                .join(args[karg]);
        }
        return newCode;
    }
}

const bounder = (() => {
    let bounder = null;
    downloadBounder('js', Gjs).then(b => {
        bounder = b;
        test();
    });
    return args => {
        if(bounder === null){
            throw 'Too quick';
        }
        return bounder(args);
    }
})();

/**
 * 
 * @param {Rule[]} rules Parsed rules
 * @param {Value} value Value for init
 * @param {Action} action Action, you asshole
 * @returns string
 */
function strTrace(rules, value, action){
    const startAction = action ?? initAction;
    const walked = {};
    const firstWalk = strTraceRec(
        rules,
        value,
        startAction,
        {label: [], walked},
    ).join('\n');
    // console.skip('First walk >>', firstWalk);
    walked[strAction(startAction)]++;
    // console.log('Found actions >>', walked);
    const ctx = {
        alreadyWalked: walked,
        walked: {},
        walkedActions: {},
        simpleActionJoin: {},
        cnt: {
            values: 0,
            labels: 0,
        },
        g: Gjs,
    };
    const mainLoopCode = strWalkRec(
        rules,
        value,
        startAction,
        ctx
    ).join(ctx.g.delim);
    // console.log('Switch >>', mainLoopCode);
    const varsCode = Or(ctx.cnt.values + 1)
        .flatMap(v => ctx.g.varidef(v))
        .join(ctx.g.delim);
    const lblsCode = Or(ctx.cnt.labels + 1)
        .flatMap(v => ctx.g.lbldef(v))
        .join(ctx.g.delim);
    const allCode = bounder({
        mainLoopCode, varsCode, lblsCode,
    });
    // console.log('All code >>', allCode);
    return allCode;
}

const testCode = `
// all

@txt.text.o => $v !== "" => @clr.isVisible
@txt.text.o => => result:await.last.i
@txt.text.o => $v.split(";") => @lst.i
@clr.click => "" => @txt.text.i
@srch.click => => result:await.last.e
result:await.last.o => => result

@lst.o => => @lst/lbl.text.i

// txt

@txt.init => => txt1:canvas.event.onChange
txt1:canvas.event.onChange => => @txt.text.o
@txt.text.i => => canvas.setText
@txt.text.i => => @txt.text.o

// clr

@clr.init => => clr1:canvas.event.onClick
clr1:canvas.event.onClick => => @clr.click
@clr.isVisible => => canvas.setVisible

// srch

@srch.init => => srch1:canvas.event.onClick
srch1:canvas.event.onClick => => @srch.click
@srch.isVisible => => canvas.setVisible

// lst

// Смотрим на изменение длины массива
@lst.i => $v.length => lst1:await.memo.i
@lst.init => 0 => lst1:await.memo.v
// Массив надо расширять?
lst1:await.memo.o => $v.old < $v.new => lst2:await.if.i
lst1:await.memo.o => => lst2:await.if.v
// Индексы новых элементов
lst2:await.if.o => range($v.old, $v.new, $v) => lst4:arr.each.i
// Создаем элемент, подменили @lst/lbl
lst4:arr.each.o => => @lst/lbl.clone
@lst/lbl.clone => log($v, 'cloning') => lst5:canvas.clone
// Нам нужно сохранить контекст подмены
lst5:canvas.clone => => lst6:await.last.c
// Вызываем обработчик
lst6:await.last.o => => @lst.o
// Делим массив
@lst.i => => lst7:arr.each.i
// Прокидываем значения для обработчика и зовем на изменение массива
lst7:arr.each.o => => lst6:await.last.i
lst7:arr.each.o => => lst6:await.last.e
// Массив надо сужать?
lst1:await.memo.o => $v.old > $v.new => lst8:await.if.i
lst1:await.memo.o => => lst8:await.if.v
// Индексы элементов на удаление
lst8:await.if.o => range($v.new, $v.old, $v) => lst9:arr.each.i
// Берем контекст созданных строк для удаления
lst5:canvas.clone => => lst10:await.last.c
// Пробрасываем индекс созданных строк
lst5:canvas.clone => => lst11:await.any.i.actual
// Пробрасываем индекс события удаления в условие
lst9:arr.each.o => => lst11:await.any.i.given
// Собственно условие
lst11:await.any.o => $v.actual === $v.given => lst12:await.if.i
// Зовем удаление
lst12:await.if.o => => last10:await.last.e
// Собственно удаление
lst10:await.last.o => => @lst/lbl.remove
@lst/lbl.remove => log($v, 'removing') => canvas.remove

// lst/lbl

@lst/lbl.text.i => => canvas.setText

// init

init => => @txt.init
init => => @clr.init
init => => @srch.init
init => => @lst.init
`;

export function test(){
    const rules = parseRules(parseCode(testCode));
    // console.log('All rules >>', rules);
    const code = strTrace(rules, 'null');
    // console.log('Macrosed rules >>', rules.filter(rule => !!rule.macros));
    // blob source dont supported
    const i = createBase64Url(testCode);
    // it dont need actually but here we are
    const o = createBlobUrl(code);
    const sourceMap = getSourceMap(code, Gjs, {
        inputUrl: i,
        outputUrl: o,
    });
    // console.log('Source map >>', sourceMap);
    // blob source maps dont supported
    const io = createBase64Url(sourceMap);
    const codeWithMap = code + `\n//# sourceMappingURL=${io}`;
    const ioo = createBlobUrl(codeWithMap);
    // import 'o' istead of 'ioo' to avoid source mapping
    import(ioo).then(m => {
        m.default();
    });
}
