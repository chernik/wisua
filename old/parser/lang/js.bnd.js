// Variables

/* [[[ varsCode ]]] */

// Labels

/* [[[ lblsCode ]]] */

const LL = -1;

// TODO: remove

const elements = {
    root: document.createElement('div'),
    txt: document.createElement('input'),
    clr: document.createElement('button'),
    srch: document.createElement('button'),
    'lst/lbl': document.createElement('input'),
};

elements.root.appendChild(elements.txt);
elements.root.appendChild(elements.clr);
elements.root.appendChild(document.createElement('br'));
elements.root.appendChild(elements.srch);
elements.root.appendChild(elements['lst/lbl']);

elements.clr.innerText = 'CLR';
elements.srch.innerText = 'SRCH';
elements.root.style = `
width: 200px;
position: absolute;
top: 200px;
left: 50px;
height: 50px;
`;

document.body.appendChild(elements.root);

// End of remove

const events = [];
const listener = [];
let nextLabel = L0;

// Macroses

async function* canvas(...args){
    const ge = () => elements[args[1]];
    switch(args[0]){
        case 'event.onChange':
            while(true){
                await new Promise(cb => ge().onkeydown = () => setTimeout(cb));
                yield ge().value;
            }
        case 'event.onClick':
            while(true){
                yield await new Promise(cb => ge().onclick = cb);
            }
        case 'setVisible':
            ge().style.display = args[2] ? '' : 'none';
            return;
        case 'setText':
            ge().value = args[2];
            return;
        default:
            console.log('canvas !!!', args);
            return;
    }
}

const awaits = {};
{
    ['last', 'memo', 'if'].forEach(arg => {
        awaits[arg] = {
            vs: {},
            cbs: {},
        };
    });
}

function setAwait(arg, mrk, v){
    // console.warn(`${arg}.${mrk} << ${v} like ${v?.old} > ${v?.new}`);
    awaits[arg].vs[mrk] = v;
    awaits[arg].cbs[mrk]?.forEach(cb => cb());
    delete awaits[arg].cbs[mrk];
}

async function getAwait(arg, mrk, noRemove){
    if(mrk in awaits[arg].vs){
        const v = awaits[arg].vs[mrk];
        if(!noRemove){
            delete awaits[arg].vs[mrk];
        }
        delete awaits[arg].cbs[mrk];
        // console.warn(`${arg}.${mrk} >> ${v} like ${v?.old} > ${v?.new} as ${noRemove}`);
        return v;
    }
    await new Promise(cb => {
        if(mrk in awaits[arg].cbs){
            awaits[arg].cbs[mrk].push(cb);
        } else {
            awaits[arg].cbs[mrk] = [cb];
        }
    });
    return await getAwait(arg, mrk, noRemove);
}

async function* _await(...args){
    const [mrk, n] = args[1].split(':');
    switch(args[0]){
        case 'last': {
            switch(n){
                case 'i': {
                    setAwait('last', mrk, args[2]);
                    return;
                } case 'e': {
                    yield await getAwait('last', mrk);
                    return;
                } case 'c': {
                    console.warn('CTX not implemented');
                    return;
                } default:
                    break;
            }
        } case 'memo': {
            switch(n){
                case 'v': {
                    setAwait('memo', mrk, args[2]);
                    return;
                } case 'i': {
                    const prev = await getAwait('memo', mrk, true);
                    if(prev === args[2]){
                        return;
                    }
                    setAwait('memo', mrk, args[2]);
                    yield {
                        new: args[2],
                        old: prev,
                    };
                    return;
                } default:
                    break;
            }
        } case 'if': {
            switch(n) {
                case 'v': {
                    setAwait('if', mrk, args[2]);
                    return;
                } case 'i': {
                    const v = await getAwait('if', mrk);
                    if(args[2]){
                        yield v;
                    }
                    return;
                } default:
                    break;
            }
        } default:
            break;
    }
    console.log('await !!!', args);
}

async function* arr(...args){
    const [_, n] = args[1].split(':');
    switch(args[0]){
        case 'each': {
            switch(n){
                case 'i': {
                    for(let el of args[2]){
                        yield el;
                    }
                    return;
                } default:
                    break;
            }
        } default:
            break;
    }
    console.log('arr !!!', args);
}

function range(a, b, c) {
    console.log('range called with', a, b);
    const res = [];
    for(let i = a; i < b; i++){
        res.push(i);
    }
    return res;
}

function log(...args){
    console.log('LOG >>', ...args);
    return args[0];
}

// End of macroses

async function takeEvery(gen, lbl){
    while(true){
        const {done, value: event} = await gen.next();
        if(done){
            break;
        }
        events.push({
            lbl, event, gen,
        });
        listener[0]?.();
    }
}


function v(gen){
    return gen._event;
}

async function noret(gen){
    const {done} = await gen.next();
    // console.log('Noret...');
    if(!done){
        throw 'Expect one call';
    }
}

async function getNextLabel(){
    if(events.length === 0){
        await new Promise(cb => listener[0] = cb);
    }
    if(events.length === 0){
        throw 'wtf2';
    }
    // May be >= 2 cuz some generators may produce many events in sync thread part
    const {
        event, lbl, gen,
    } = events.shift();
    // console.log('Event >>', event);
    gen._event = event;
    return lbl;
}

export default async function main(){
    while(true){
        // console.log('Iteration');
        switch(nextLabel){
            case L0:
                /* [[[ mainLoopCode ]]] */
            case LL:
                nextLabel = await getNextLabel();
                break;
            default:
                throw `Unexpected label: ${nextLabel}`;
        }
    }
}
