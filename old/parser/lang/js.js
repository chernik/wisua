import {tabSpace} from '../utils.js';

export default {
    assign(v, val){
        return [`${tabSpace}V${v} = ${val};`];
    },
    comment(code, comm){
        if(!code){
            return [`// ${comm}`];
        }
        return [
            `${code[0]} // ${comm}`,
            ...code.slice(1),
        ];
    },
    goto(lbl){
        return [
            `${tabSpace}nextLabel = L${lbl};`,
            `${tabSpace}break;`,
        ];
    },
    lbl(lbl){
        return [`case L${lbl}:`];
    },
    takeEvery(v, lbl){
        return [
            `${tabSpace}takeEvery(V${v}, L${lbl});`,
        ];
    },
    noret(v){
        return [`${tabSpace}noret(${v});`];
    },
    sourceMapMarker(origLineNumber){
        return [`// [[[ origLineNumber [${origLineNumber}] ]]]`];
    },
    parseSourceMap(s){
        return Number(s.split(['[[[ origLineNumber ['])[1]?.split('] ]]]')[0] ?? '-1');
    },
    // ---
    _def(pref, id){
        return [`const ${pref}${id} = ${id};`];
    },
    varidef(v){
        return [`let V${v};`];
    },
    lbldef(lbl){
        return [`const L${lbl} = ${lbl};`];
    },
    // ---
    varival(code){
        return `v(${code})`;
    },
    vari(v){
        return `V${v}`;
    },
    bndCmnt(argk){
        return `/* [[[ ${argk} ]]] */`;
    },
    delim: '\n',
}