import {LinkType, getOutputAction as getOutput} from '../utils.js';
import Unknown from './unknown.js';
import IO from './io.js';

export default (action, ctx) => {
    const [res, loc] = IO(action);

    if(res !== null){
        return res;
    }

    const {what, key, ctxAction} = loc;

    switch(what) {
        case 'each':
            switch(key) {
                case 'i':
                    return {
                        apply: v => `arr("each", ${ctxAction}, ${v})`,
                        type: LinkType.Multiple,
                        action: getOutput(action),
                    }
                default:
                    console.log('for each', key);
                    return Unknown(action);
            }
        default:
            console.log(what, key);
            return Unknown(action);
    }
}
