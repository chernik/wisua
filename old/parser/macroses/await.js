import {LinkType, getOutputAction as getOutput} from '../utils.js';
import Unknown from './unknown.js';
import IO from './io.js';

export default (action, ctx) => {
    const [res, loc] = IO(action);

    if(res !== null){
        return res;
    }

    const strCtx = `"${ctx.label.join('/')}"`;
    const {what, key, ctxAction} = loc;

    switch(what) {
        case 'last':
            switch(key) {
                case 'i':
                    return {
                        apply: v => `_await("last", ${ctxAction}, ${v})`,
                        type: LinkType.Leaf,
                        action: null,
                    }
                case 'c':
                    return {
                        apply: v => `_await("last", ${ctxAction}, ${strCtx})`,
                        type: LinkType.Leaf,
                        action: null,
                    };
                case 'e':
                    return {
                        apply: v => `_await("last", ${ctxAction})`,
                        type: LinkType.Condition,
                        action: getOutput(action),
                    }
                default:
                    console.log('for last', key);
                    return Unknown(action);
            }
        case 'memo':
            switch(key) {
                case 'i':
                    return {
                        apply: v => `_await("memo", ${ctxAction}, ${v})`,
                        type: LinkType.Condition,
                        action: getOutput(action),
                    }
                case 'v':
                    return {
                        apply: v => `_await("memo", ${ctxAction}, ${v})`,
                        type: LinkType.Leaf,
                        action: null,
                    }
                default:
                    console.log('for memo', key);
                    return Unknown(action);
            }
            case 'if':
                switch(key) {
                    case 'i':
                        return {
                            apply: v => `_await("if", ${ctxAction}, ${v})`,
                            type: LinkType.Condition,
                            action: getOutput(action),
                        }
                    case 'v':
                        return {
                            apply: v => `_await("if", ${ctxAction}, ${v})`,
                            type: LinkType.Leaf,
                            action: null,
                        }
                    default:
                        console.log('for if', key);
                        return Unknown(action);
                }
            case 'any': {
                // Calls on any of input is changed
                // It's not impossible to call output with each input equals undefined
                // At least one input will be not null (iniciator)
                // It neccecary to check it while use it
                switch(key) {
                    case 'i':
                        return {
                            apply: v => `_await("any", ${ctxAction}, ${v})`,
                            type: LinkType.Condition,
                            action: getOutput(action),
                        };
                    default:
                        console.log('for any', key);
                        return Unknown(action);
                }
            }
        default:
            console.log(what, key);
            return Unknown(action);
    }
}
