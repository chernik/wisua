import {strAction, LinkType} from '../utils.js';
import Unknown from './unknown.js';

export default (action, ctx) => {
    const subAction = {
        mark: null,
        notesLabels: action.notesLabels.slice(1),
    };

    const strCtx = `"${ctx.label.join('/')}"`;
    const nextAction = action.mark === null ? null : action;

    switch(strAction(subAction)) {
        case 'event.onChange':
            return {
                apply: v => `canvas("event.onChange", ${strCtx})`,
                type: LinkType.Multiple,
                action,
            };
        case 'event.onClick':
            return {
                apply: v => `canvas("event.onClick", ${strCtx})`,
                type: LinkType.Multiple,
                action,
            };
        case 'setVisible':
            return {
                apply: v => `canvas("setVisible", ${strCtx}, ${v})`,
                type: LinkType.One,
                action: nextAction,
            };
        case 'setText':
            return {
                apply: v => `canvas("setText", ${strCtx}, ${v})`,
                type: LinkType.One,
                action: nextAction,
            };
        case 'clone':
            return {
                apply: v => `canvas("clone", ${strCtx}, ${v})`,
                type: LinkType.One,
                action: nextAction,
            }
        default:
            console.log('for canvas', strAction(subAction));
            return Unknown(action);
    }
}
