import CanvasMacros from './canvas.js';
import AwaitMacros from './await.js';
import ArrMacros from './arr.js';

export default macrosName => {
    switch(macrosName) {
        case 'canvas': return CanvasMacros;
        case 'await': return AwaitMacros;
        case 'arr': return ArrMacros;
        default: return null;
    }
}
