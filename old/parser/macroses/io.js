import {strAction, LinkType, getOutputAction as getOutput} from '../utils.js';
import Unknown from './unknown.js';

export default action => {
    const what = action.notesLabels[1]?.note ?? null;
    const key = action.notesLabels[2]?.note ?? null;

    if(what === null || key === null){
        return [Unknown(action), null];
    }

    const ctxActionObject = {
        mark: action.mark,
        notesLabels: action.notesLabels.slice(2),
    };

    const ctxAction = `"${strAction(ctxActionObject)}"`;

    return [null, {what, key, ctxAction, ctxAction}];
}
