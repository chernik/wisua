import {strAction, LinkType} from '../utils.js';

export default (action) => {
    return {
        apply: v => `unknown(${v})`,
        type: LinkType.Unknown,
        // TODO do we need to continue for unknown?
        action: action.mark === null ? null : action,
    };
}
