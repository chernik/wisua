/**
 * 
 * @param {Action} action Action to serialize
 * @returns string
 */
export function strAction(action){
    const smark = action.mark ? `${action.mark}:` : '';
    const snotesLabels = action
        .notesLabels
        .map(noteLabel => noteLabel.note ? noteLabel.note : `@${noteLabel.label.join('/')}`)
        .join('.');
    return `${smark}${snotesLabels}`;
}

export const LinkType = {
    Multiple: 'Multiple',
    Unknown: 'Unknown',
    One: 'One',
    Leaf: 'Leaf',
    Condition: 'Contidion',
};

export function getOutputAction(action){
    return {
        ...action,
        notesLabels: [
            ...action.notesLabels.slice(
                0,
                2
            ),
            {note: 'o'},
        ],
    };
}

export const tabSpace = '\t';

export async function downloadContent(url){
    const x = new XMLHttpRequest();
    x.open('GET', url, true);
    x.send();
    await new Promise(cb => x.onload = cb);
    return x.responseText;
}

export function createBlobUrl(fileContent){
    const f = new File([fileContent], 'blob.js', {
        type: 'text/javascript',
    });
    const u = URL.createObjectURL(f);
    return u;
}

export function createBase64Url(fileContent){
    const base64 = btoa(unescape(encodeURIComponent(fileContent)));
    const mimeType = '';
    const url = `data:${mimeType};base64,${base64}`;
    return url;
}

export function getSourceMap(code, g, {inputUrl, outputUrl}){
    const lines = code.split(g.delim);
    let lastI = 0;
    const mappings = lines.map((line, o) => {
        const i = g.parseSourceMap(line);
        if(i < 0){
            return '';
        }
        const res = vlq.encode([
            0, // Column of Output
            0, // File index (look https://github.com/Rich-Harris/vlq/tree/master/sourcemaps for more details)
            i - lastI, // Line of Input
            0, // Column of Input
        ]);
        lastI = i;
        return res;
    }).join(';');
    return JSON.stringify({
        version: 3,
        sources: [inputUrl],
        names: [],
        mappings,
        file: outputUrl,
    });
}
