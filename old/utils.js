export function Ostl(el, stls){
    for(let s in stls){
        el.style[s] = stls[s];
    }
}

export function Och(el, parentEl){
    parentEl.appendChild(el);
}

export function One(tag, parentEl){
    const el = document.createElement(tag);
    if(parentEl){
        Och(el, parentEl);
    }
    return el;
}

export async function openFile({accept}){
    var inp = One('input');
    inp.type = 'file';
    if(accept){
        inp.accept = accept.map(ex => `.${ex}`).join(', ');
    }
    inp.click();
    await new Promise(cb => {
        inp.onchange = cb;
    });
    return Array.from(inp.files);
}

export async function getImgData(imgFile){
    const url = URL.createObjectURL(imgFile);
    const name = `pic${new Date().getTime()}.png`;
    const img = One('img');
    img.src = url;
    await new Promise(cb => {
        img.onload = img.onerror = cb;
    });
    if(img.width === 0 || img.height === 0){
        alert('Wrong image');
        return null;
    }
    return {
        name,
        file: imgFile,
        url,
        width: img.width,
        height: img.height,
    };
}
