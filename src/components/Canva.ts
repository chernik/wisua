import {One, Och, Ostl} from '../utils/index.js';
import {Component as ComponentModel, Instance} from '../models.js';
import {Okeys, Or} from '../objs.js';
import {TextMenu} from './Menu.js';
import type { BaseObj, Callback, ComponentData, ComponentProps, RootState, StyleProps } from '../utils/types';

function getImgUrl(state: RootState, imgName?: string){
  if(!imgName){
    return null;
  }
  const imgs = state.files.filter(
    img => img.name === imgName
  );
  if(imgs.length === 0){
    alert(`Not found file [${imgName}]`);
    return null;
  }
  return imgs[0].url;
}

function findComponents(state: RootState, name: string){
  return state.components.filter(
    comp => comp.name === name
  );
}

function addStyle(
  state: RootState,
  div: HTMLElement,
  styles: {
    img?: StyleProps['img'],
    text?: StyleProps['text'],
    list?: StyleProps['list'],
  },
  {component, render}: {
    component: ComponentProps,
    render: (data: ComponentData) => HTMLElement,
  },
){
  if(styles.img){
    const url = getImgUrl(state, styles.img.name);
    if(url){
      Ostl(div, {
        backgroundImage: `url(${url})`,
        backgroundSize: '100% 100%',
      })
    }
  }
  if(styles.text){
    div.innerText = styles.text.text ?? '';
    // div.contenteditable = styles.text.editable;
    Ostl(div, {
      fontSize: '18px',
      color: '#151633',
      fontFamily: 'cursive',
      paddingLeft: '7px',
      paddingTop: '7px',
      boxSizing: 'border-box',
    });
  }
  if(styles.list){
    if(component.children.length === 0){
      div.innerText = 'Add a child for magic';
      return;
    }
    if(component.children.length > 1){
      div.innerText = 'Expected exact one child';
    }
    const data = component.children[0];
    data.x = data.y = 0;
    [data, ...Or(4).map(i => ({
      ...data,
      x:  styles.list!.isVertical ? 0 : (i+1) * (styles.list!.step ?? 0),
      y: !styles.list!.isVertical ? 0 : (i+1) * (styles.list!.step ?? 0),
    }))].map(render).forEach((el, i) => {
      Ostl(el, {
        opacity: `${100 - i * 85 / 5}%`,
      });
      Och(el, div);
    });
    return false;
  }
  return true;
}

interface ComponentState {
  data: ComponentData & {
    upd?: Callback[],
  },
  component: ComponentProps,
}

type ComponentRender<D> = (data: D) => HTMLElement;

function Component(
  gs: RootState,
  {data, component}: ComponentState,
  render: ComponentRender<ComponentData>,
){
  const UIdiv = One('div');
  Ostl(UIdiv, {
    width: `${component.width}px`,
    height: `${component.height}px`,
    position: 'absolute',
  });
  function upd(){
    Ostl(UIdiv, {
      left: `${data.x}px`,
      top: `${data.y}px`,
    });
  }
  upd();
  data.upd = data.upd || [];
  data.upd.push(upd);
  if(addStyle(gs, UIdiv, data.styles, {component, render})){
    component.children.map(render).forEach(el => {
      Och(el, UIdiv);
    });
  }
  return {r: () => UIdiv};
}

interface DragComponentHandlers {
  onMove: () => void;
  onProps: () => void;
  onRemove: () => void;
  onStyles: () => void;
}

function DragComponent(
  gs: RootState,
  state: ComponentState,
  render: ComponentRender<ComponentData>,
  {onMove, onProps, onRemove, onStyles}: DragComponentHandlers,
){
  const component = Component(gs, state, render);
  const div = component.r();
  Ostl(div, {
    border: '1px solid blue',
  });
  div.oncontextmenu = e => {
    if(e.ctrlKey){
        onRemove();
    } else if(e.shiftKey){
        onStyles();
    } else {
        onProps();
    }
    e.stopPropagation();
    e.preventDefault();
  }
  function upd(){
    Ostl(div, {
      left: `${state.data.x}px`,
      top: `${state.data.y}px`,
    });
    state.data.upd?.forEach(cb => cb());
  }
  const lastCoor: [null | [number, number]] = [null];
  div.onmousedown = e => {
      lastCoor[0] = [e.clientX, e.clientY];
      window.addEventListener('mousemove', mouseMove);
      window.addEventListener('mouseup', mouseUp);
  }
  function mouseUp(){
      lastCoor[0] = null;
      window.removeEventListener('mousemove', mouseMove);
      window.removeEventListener('mouseup', mouseUp);
  }
  function mouseMove(e: MouseEvent){
    if (!lastCoor[0]) {
      return;
    }
    const newCoor = [e.clientX, e.clientY];
    state.data.x += newCoor[0] - lastCoor[0][0];
    state.data.y += newCoor[1] - lastCoor[0][1];
    lastCoor[0] = [e.clientX, e.clientY];
    onMove();
    upd();
  }
  div.onclick = e => e.stopPropagation();
  div.onselectstart = () => false;
  return component;
}

function RootComponent(gs: RootState, state: ComponentState, render: ComponentRender<ComponentData>){
  const component = Component(gs, state, render);
  Ostl(component.r(), {
    margin: '20px',
    border: '1px solid black',
    left: 'auto',
    top: 'auto',
  });
  return component;
}

function errorComponent({data}: Pick<ComponentState, 'data'>, render: ComponentRender<ComponentState>, msg: string){
  const state = {
    data,
    component: ComponentModel({
      width: 100,
      height: 100,
    }),
  };
  const div = render(state);
  Ostl(div, {
    backgroundColor: 'red',
  });
  div.innerText = msg;
  return div;
}

function recurseComponent(
  {data, component: baseComponent}: ComponentState,
  render: ComponentRender<ComponentState>,
){
  const state = {
    data,
    component: ComponentModel({
      width: baseComponent.width,
      height: baseComponent.height,
    }),
  };
  const div = render(state);
  Ostl(div, {
    background: 'radial-gradient(ellipse, #fff 0%, #000 30%, #fff0 60%)',
  });
  div.title = 'Recursion detected';
  return div;
}

export default (state: RootState, onUpdate: Callback) => {
  const UI: {
    render?: (data: ComponentData, render?: ComponentRender<ComponentState>) => HTMLElement,
    upd?: Callback,
  } = {};
  const UIdiv = One('div');
  UI.render = (data, render) => {
    render = render || (subState => Component(
      state, subState,
      data => UI.render!(data)
    ).r());
    const {name} = data;
    const components = findComponents(state, name);
    if(components.length === 0){
      return errorComponent({data}, render, `Not found [${name}]`);
    }
    if(components.length === 2){
      return errorComponent({data}, render, `Duplicate [${name}]`);
    }
    const subState = {data, component: components[0]};
    if(state.graph?.has(name)){
      if((state.loopCount ?? 0) > 7){
        return recurseComponent(subState, render);
      }
      state.loopCount = (state.loopCount ?? 0) + 1;
    }
    state.graph?.add(name);
    const bufRec = {
      graph: Array.from(state.graph ?? []),
      loopCount: state.loopCount,
    };
    const div = render(subState);
    state.graph = new Set(bufRec.graph);
    state.loopCount = bufRec.loopCount;
    return div;
  };
  UI.upd = () => {
    if (!UI.render) {
      return;
    }
    if(state.choose === undefined){
        UIdiv.innerText = '<< Select component';
        return;
    }
    state.graph = new Set();
    state.loopCount = 0;
    const component = state.components[state.choose];
    const rootData = Instance(
      component,
      0, 0
    );
    UIdiv.innerHTML = '';
    const mainComp = UI.render(rootData, rootState =>
      RootComponent(state, rootState, dragData =>
        UI.render!(dragData, dragState =>
          DragComponent(
            state, dragState,
            data => UI.render!(data),
            {
              onProps(){
                state.forEditor = {obj: dragData, type: 'Instance'};
                onUpdate();
              },
              onRemove(){
                const idx = component.children.findIndex(
                  adata => adata === dragData
                );
                if(idx < 0){
                  return;
                }
                component.children.splice(idx, 1);
                UI.upd!();
                onUpdate();
              },
              async onStyles(){
                const styles = Okeys(dragData.styles);
                if(styles.length === 0){
                  alert('There is no styles');
                  return;
                }
                const idx = await TextMenu(styles)();
                if(idx === null){
                  return;
                }
                const style = dragData.styles[styles[idx]];
                state.forEditor = {obj: style as BaseObj, type: 'Style'};
                onUpdate();
              },
              onMove: onUpdate,
            }
          ).r()
        )
      ).r()
    );
    delete state.graph;
    delete state.loopCount;
    mainComp.onclick = () => {
      state.forEditor = {obj: component, type: 'Component'};
      onUpdate();
    }
    mainComp.oncontextmenu = e => {
      const name = prompt('>');
      if(name){
        const components = findComponents(state, name);
        component.children.push(Instance(
          components.length === 1
            ? components[0]
            : ComponentModel({name}),
          e.offsetX,
          e.offsetY
        ));
        UI.upd!();
        onUpdate();
      }
      e.preventDefault();
    }
    Och(mainComp, UIdiv);
  };
  UI.upd();
  return {r: () => UIdiv, upd: UI.upd};
}
