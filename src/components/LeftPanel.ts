import { One, Och, Ostl, openFile, getImgData } from '../utils/index.js';
import { Component } from '../models.js';
import type { Callback, ComponentProps, RootState } from '../utils/types';

function simpleBut(el: HTMLElement, t: string, cb: Callback){
  const but = One('button', el);
  but.innerText = t;
  Ostl(but, {
    width: '20px',
    height: '20px',
    marginLeft: '3px',
  });
  but.onclick = cb;
  return but;
}

interface LeftPanelItemCallbacks {
  onChoose: () => void;
  onRemove: () => void;
  onUpdate: () => void;
  isChoose: boolean;
}

function LeftPanelItem(
  component: ComponentProps,
  {onChoose, onRemove, onUpdate, isChoose}: LeftPanelItemCallbacks,
){
  const UI: {
    upd?: Callback;
  } = {};
  const UIdiv = One('div');
  Ostl(UIdiv, {
    display: 'flex',
    marginTop: '2px',
  });
  const UIname = One('span', UIdiv);
  Ostl(UIname, {
    width: '100%',
    borderBottom: '1px solid black',
  });
  if(isChoose){
    Ostl(UIname, {
      backgroundColor: '#dddddd',
    });
  }
  UIname.onclick = () => {
    onChoose();
  };
  UIname.oncontextmenu = e => {
    const newName = prompt('>', component.name);
    if(newName){
      component.name = newName;
      UI.upd?.();
      onUpdate();
    }
    e.preventDefault();
  };
  simpleBut(UIdiv, 'X', () => {
    onRemove();
  });
  UI.upd = () => {
    UIname.innerText = component.name;
  };
  UI.upd();
  return {r: () => UIdiv};
}

function NewItem(state: RootState, onNew: Callback){
  const UIdiv = One('div');
  simpleBut(UIdiv, '+', () => {
    state.components.push(Component());
    onNew();
  });
  simpleBut(UIdiv, 'P', async () => {
    const files = await openFile({accept: ['png']});
    const img = await getImgData(files[0]);
    if(img === null){
      return;
    }
    state.files.push(img);
    state.components.push(Component({
      styles: 'img',
      staticStyles: {
        img: {name: img.name},
      },
      width: img.width,
      height: img.height,
    }));
    onNew();
  });
  return {r: () => UIdiv};
}

export default (state: RootState, onUpdate: Callback) => {
  const UI: {
    upd?: Callback;
  } = {};
  const UIdiv = One('div');
  const UInewItem = NewItem(state, () => {
    UI.upd?.();
    onUpdate();
  }).r();
  Och(UInewItem, UIdiv);
  const UIitemsCont = One('div', UIdiv);
  UI.upd = () => {
    UIitemsCont.innerHTML = '';
    state.components.forEach((component, i) => {
      const item = LeftPanelItem(component, {
        onChoose(){
          state.choose = i;
          UI.upd?.();
          onUpdate();
        },
        onRemove(){
          state.components.splice(i, 1);
          if(i === state.choose){
            delete state.choose;
          }
          if(state.choose && i < state.choose){
            state.choose--;
          }
          UI.upd?.();
          onUpdate();
        },
        onUpdate,
        isChoose: i === state.choose,
      }).r();
      Och(item, UIitemsCont);
    });
  }
  return {r: () => UIdiv, upd: UI.upd};
}
