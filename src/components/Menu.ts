import {Och, One, Ostl} from '../utils/index.js';

function BaseMenu(elements: HTMLElement[], parentEl: HTMLElement){
  const bgDiv = One('div');
  Ostl(bgDiv, {
    position: 'fixed',
    width: '100%',
    height: '100%',
    backgroundColor: '#3333334d',
    zIndex: '10',
    backdropFilter: 'blur(2px)',
  } as any);
  const div = One('div');
  Ostl(div, {
    position: 'fixed',
    display: 'flex',
    padding: '20px',
    flexDirection: 'column',
    width: 'calc(100% - 40px)',
    height: 'calc(100% - 40px)',
    alignItems: 'center',
    justifyContent: 'space-between',
    zIndex: '10',
  });
  const loc: {
    waiter: (i: number | null) => void;
  } = {
    waiter: () => 0,
  };
  elements.forEach((element, i) => {
    Och(element, div);
    element.onclick = () => {
      loc.waiter(i);
    };
  });
  function onEsc(e: KeyboardEvent){
    if(e.keyCode !== 27){
      return;
    }
    loc.waiter(null);
  }
  async function showMenu(){
    Och(bgDiv, parentEl);
    Och(div, parentEl);
    window.addEventListener('keydown', onEsc);
    for(let i = 0; i <= 100; i += 10){
      Ostl(div, {
        left: `${100 - i}%`,
      });
      Ostl(bgDiv, {
        opacity: `${i}%`,
      });
      await new Promise(cb => setTimeout(cb, 10));
    }
  }
  async function hideMenu(){
    window.removeEventListener('keydown', onEsc);
    for(let i = 0; i <= 100; i += 10){
      Ostl(div, {
        left: `-${i}%`,
      });
      Ostl(bgDiv, {
        opacity: `${100 - i}%`,
      });
      await new Promise(cb => setTimeout(cb, 10));
    }
    parentEl.removeChild(div);
    parentEl.removeChild(bgDiv);
  }
  return async () => {
    showMenu();
    const idx = await new Promise<number | null>(waiter => {
      loc.waiter = waiter;
    });
    hideMenu();
    return idx;
  };
}

const Menu = (() => {
  const parentEl = One('div');
  Och(parentEl, document.body);
  return (elements: HTMLElement[]) => BaseMenu(elements, parentEl);
})();

function stylify(element: HTMLElement){
  Ostl(element, {
    backgroundColor: '#80c9ea',
    minWidth: '300px',
    height: '30px',
    borderRadius: '5px',
    border: '2px solid #12648a',
    textAlign: 'center',
    fontSize: '22px',
    paddingTop: '4px',
    paddingLeft: '10px',
    paddingRight: '10px',
  });
  return element;
}

function fromText(text: string){
  const el = One('div');
  el.innerText = text;
  return el;
}

export const TextMenu = (texts: string[]) => Menu(
  texts
  .map(fromText)
  .map(stylify)
);
