import { One, Och, Ostl } from '../utils/index.js';
import LeftPanel  from './LeftPanel.js';
import Canva from './Canva.js';
import Editor from './Editor.js';
import type { Callback, RootState } from '../utils/types';

type ChildType = (state: RootState, onUpdate: Callback) => {
  r: () => HTMLElement,
  upd: Callback,
};

export default (state: RootState) => {
  const UIparent = One('div');
  Ostl(UIparent, {
    display: 'flex',
    height: '100%',
  });
  const UIchilds: ReturnType<ChildType>[] = [];
  function addChild(Child: ChildType, width: string){
    const child = Child(state, () =>
      // Обновляем все компоненты, если какой нить обновился
      UIchilds.forEach(ch => ch !== child && ch.upd())
    );
    UIchilds.push(child);
    const div = child.r();
    Och(div, UIparent);
    Ostl(div, {width});
  }
  addChild(LeftPanel, '170px');
  addChild(Canva, 'calc(100% - 470px)');
  addChild(Editor, '300px');
  return {r: () => UIparent};
}
