import Page from './components/Page.js';
import type { RootState } from './utils/types';
// TODO using for testing for a while
// import {test} from './parser/graph.js';

window.onload = () => {
  const state: RootState = {
    components: [],
    files: [],
  };
  document.body.appendChild(Page(state).r());
}
