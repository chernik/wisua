import {flatCopy} from './objs.js';
import type { Lens } from './utils/types';

function connectTwoLens<L1, L2, L3>(l1: Lens<L1, L2>, l2: Lens<L2, L3>): Lens<L1, L3>{
  return {
    s(o: L1){
      const r = l1.s(o);
      return r === null ? null : l2.s(r);
    },
    g(o: L3){
      const r = l2.g(o);
      return r === null ? null : l1.g(r);
    },
  };
}

export function connectLens<Lenses extends Lens<any, any>[]>(lenses: Lenses){
    if(lenses.length === 1){
      return lenses[0];
    }
    let lens = null;
    for(let i = lenses.length - 1; i > 0; i--){
      lens = lens === null
        ? connectTwoLens(lenses[i-1], lenses[i])
        : connectTwoLens(lenses[i-1], lens);
    }
    return lens;
}

export const jsonLens: Lens<unknown, string> = {
  s: obj => JSON.stringify(obj, null, 4),
  g(s){
    try {
      return JSON.parse(s);
    } catch(e) {
      return null;
    }
  }
};

export function lazyLens<Obj extends Record<string, unknown>>(filter?: (obj: Obj) => Obj){
  let buf: [Obj, string] | [] = [];
  return {
    s(obj: Obj){
      const copy = filter ? filter(obj) : obj;
      if(buf.length !== 0 && JSON.stringify(copy) === buf[1]){
        return null;
      }
      buf[0] = obj;
      buf[1] = JSON.stringify(copy);
      return copy;
    },
    g(obj: Obj){
      if(buf.length === 0){
        return null;
      }
      if(flatCopy(obj, buf[0]) === null){
        return null;
      }
      buf[1] = JSON.stringify(obj);
      return buf[0];
    },
  }
}

export function triggerFieldLens<T>(field: string, trigger: (prevValue: T, value: T) => void){
  let buf: [] | [T] = [];
  return {
    s<O extends Record<string, unknown>>(obj: O){
      buf = [obj[field] as T];
      return obj;
    },
    g<O extends Record<string, unknown>>(obj: O){
      if(buf.length === 1 && buf[0] !== obj[field]){
        trigger(buf[0], obj[field] as T);
        buf[0] = obj[field] as T;
      }
      return obj;
    }
  }
}
