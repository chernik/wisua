import {flatCopy, updeps} from './objs.js';
import type { ComponentData, ComponentProps, StyleProps, UnknownObj } from './utils/types';

function Style<T extends keyof StyleProps>(style: T | string, props: StyleProps[T]){
  if(style === 'img'){
    const typedProps = props as StyleProps['img'];
    return {
      name: typedProps?.name || 'not_found.png',
    };
  }
  if(style === 'text'){
    const typedProps = props as StyleProps['text'];
    return {
      text: typedProps?.text || 'Some text',
      editable: typedProps?.editable || false,
    };
  }
  if(style === 'list'){
    return {
      step: 50,
      isVertical: true,
    };
  }
  return {wrongStyle: 'So wrong!'};
}

function copyStyles(s: UnknownObj, d: UnknownObj){
  return flatCopy(s, d, {
    canAddRemove: true,
    handleLevel: (s, d, k) => k === 'upd'
      ? false
      : !!flatCopy(s[k] as UnknownObj, d[k] as UnknownObj, {
        canAddRemove: true,
        canChange: false,
      }),
  });
}

export function Component(props?: Partial<ComponentProps>){
  const component: ComponentProps = {
      name: 'NewComponent',
      width: 300,
      height: 300,
      children: [],
      styles: '',
      staticStyles: {},
      ...(props || {}),
  };
  updeps(component, () => {
    const {staticStyles, styles} = component;
    const styleNames = styles
      .split(' ')
      .map(s => s.trim())
      .filter(s => s !== '');
    const realStyles: UnknownObj = {};
    styleNames.forEach(s => {
      realStyles[s] = Style(s, staticStyles[s] as any);
    });
    copyStyles(realStyles, staticStyles);
  })();
  return component;
}

export function Instance(component: ComponentProps, x: number, y: number){
  const data: ComponentData = {
    name: component.name,
    x, y,
    styles: {},
  };
  updeps(data, () => {
    const {staticStyles} = component;
    copyStyles(staticStyles, data.styles);
  })();
  if (data.modelUpdates?.upd) {
    component.modelUpdates?.upds.push(data.modelUpdates.upd);
  }
  return data;
}

export default {};
