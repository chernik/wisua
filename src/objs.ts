import type { BaseObj, Callback } from "./utils/types";

export function OcpObj<T>(o: T): T {
  return JSON.parse(JSON.stringify(o));
}

export function Okeys<T extends object>(o: T){
  const res = [];
  for(let k in o){
    res.push(k);
  }
  return res;
}

function OisT(obj: unknown, ts: string[]){
  return new Set(ts).has(typeof(obj));
}

const OisSimple = (obj: unknown) => OisT(obj, ['string', 'number', 'boolean']);

interface Args {
  canAddRemove?: boolean;
  handleLevel?: (s: Record<string, unknown>, d: Record<string, unknown>, k: string) => boolean;
  canChange?: boolean;
}

export function flatCopy(s: Record<string, unknown>, d: Record<string, unknown>, args?: Args){
  const {
    canAddRemove = false,
    handleLevel = () => false,
    canChange = true,
  } = args ?? {};
  const keys = Array.from(new Set([...Okeys(s), ...Okeys(d)]));
  return keys.filter(k => {
    if(s[k] === d[k]){
      return false;
    }
    if(s[k] === undefined){
      if(!canAddRemove){
        return false;
      }
      delete d[k];
      return true;
    }
    if(d[k] === undefined){
      if(!canAddRemove){
        return false;
      }
      if(OisSimple(s[k])){
        d[k] = s[k];
        return true;
      }
      d[k] = {};
      return handleLevel(s, d, k);
    }
    if(!canChange){
      return false;
    }
    if(typeof(s[k]) !== typeof(d[k])){
      return false;
    }
    if(!OisSimple(s[k])){
      return handleLevel(s, d, k);
    }
    d[k] = s[k];
    return true;
  }).length > 0 ? d : null;
}

export function simplifyObj(obj: Record<string, unknown>){
  const res: typeof obj = {};
  Okeys(obj).forEach(k => {
    if(!OisSimple(obj[k])){
      return;
    }
    res[k] = obj[k];
  });
  return res;
}

export function updeps(obj: BaseObj, onUpd: Callback){
  obj.modelUpdates = {
    upds: [],
    upd: () => {
      onUpd();
      obj.modelUpdates?.upds.forEach(upd => upd());
    }
  };
  return onUpd;
}

export function Or(n: number){
  return Array.from({length: n}).map((_, i) => i);
}
