import type { ElementTag } from "./types";

function Okeys<T extends object>(o: T) {
  return Object.keys(o) as (keyof T)[];
}

export function Ostl<Element extends HTMLElement>(
  el: Element,
  stls: Partial<CSSStyleDeclaration>,
){
  Okeys(stls).forEach(s => {
    if (s === 'length' || s === 'parentRule') {
      return;
    }
    el.style[s] = stls[s] as any;
  });
}

export function Och<
 Element extends HTMLElement,
 Parent extends HTMLElement,
>(el: Element, parentEl: Parent) {
  parentEl.appendChild(el);
}

export function One<
  Tag extends ElementTag,
  Parent extends HTMLElement,
>(tag: Tag, parentEl?: Parent) {
  const el = document.createElement(tag);
  if(parentEl) {
    Och(el, parentEl);
  }
  return el;
}

export async function openFile({accept}: {accept: string[]}){
  const inp = One('input');
  inp.type = 'file';
  if (accept) {
    inp.accept = accept.map(ex => `.${ex}`).join(', ');
  }
  inp.click();
  await new Promise(cb => {
    inp.onchange = cb;
  });
  return Array.from(inp.files ?? []);
}

export async function getImgData(imgFile: Blob){
  const url = URL.createObjectURL(imgFile);
  const name = `pic${new Date().getTime()}.png`;
  const img = One('img');
  img.src = url;
  await new Promise(cb => {
    img.onload = img.onerror = cb;
  });
  if(img.width === 0 || img.height === 0){
    alert('Wrong image');
    return null;
  }
  return {
    name,
    file: imgFile,
    url,
    width: img.width,
    height: img.height,
  };
}
