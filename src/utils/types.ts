export type ElementTag = keyof HTMLElementTagNameMap;
export type Callback = () => void;
export type UnknownObj = Record<string, unknown>;

export interface Lens<A, B> {
  s: (v: A) => B | null;
  g: (v: B) => A | null;
}

export interface BaseObj {
  modelUpdates?: {
    upds: Callback[];
    upd: Callback;
  };
};

export interface ComponentProps extends BaseObj {
  name: string;
  styles: string;
  staticStyles: UnknownObj,
  width: number;
  height: number;
  children: ComponentData[];
}

export interface ComponentData extends BaseObj {
  name: string;
  x: number;
  y: number;
  styles: UnknownObj;
}

export interface RootState {
  components: ComponentProps[];
  files: {
    name: string;
    url: string;
  }[];
  choose?: number;
  forEditor?: {
    type: string;
    obj: BaseObj;
  };
  graph?: Set<string>;
  loopCount?: number;
}

export interface StyleProps {
  img: {
    name?: string;
  },
  text: {
    text?: string;
    editable?: boolean;
  },
  list: {
    step?: number;
    isVertical?: boolean;
  },
}
